package org.gjt.sp.jedit.cpi220.sorting;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

public class IO {

	public static Surname[] read(String fileName){
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		int count = 0;
		
		Surname[] surnamesTemp = new Surname[88799*15]; 
		Surname[] surnames = null;

		try {

			br = new BufferedReader(new FileReader(fileName));
			br.readLine(); // Skipping title
			while ((line = br.readLine()) != null) {

		        // use comma as separator
				String[] country = line.split(cvsSplitBy);

				Surname name = new Surname();
				name.setSurname(country[0]);
				name.setPercent(new Float(country[1]));
				name.setCumulative(new Float(country[2]));
				name.setRank(new Integer(country[3]));
				
				surnamesTemp[count++] = name;
				
				
				
			}
			surnames = Arrays.copyOfRange(surnamesTemp, 0, count-1);


		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return surnames;

	}
	
	public static void write(Surname[] objects, String fileName){
		FileWriter f0 = null;
		try {
			f0 = new FileWriter(fileName);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String newLine = System.getProperty("line.separator");


		for(int i=0;i<objects.length;i++)
		{
			Surname s = objects[i];
		    try {
				f0.write(s.getSurname() + "," + s.getPercent() + "," + s.getCumulative() + "," + s.getRank() + newLine);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			f0.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
